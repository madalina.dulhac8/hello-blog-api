using hello_blog_api.Models;
using System.Collections.Generic;
using System.Linq;

namespace hello_blog_api.TestData
{
    public static class BlogPostTestData
    {
         ///<summary>Increments the Id of each blog post when a new record is created.</summary>
        private static int _id = 0;

        private static List<BlogPostModel> _blogPosts = new List<BlogPostModel>();

        public static BlogPostModel GetBlogPostById(int id)
        {
            BlogPostModel blogPostModel =_blogPosts.FirstOrDefault(blogPost => blogPost.Id == id);
            return blogPostModel;
        }

        public static List<BlogPostModel> GetAllBlogPosts()
        {
            return _blogPosts;
        }

        public static List<BlogPostModel> GetBlogPostsByTag(string tag)
        {
          if (!string.IsNullOrWhiteSpace(tag))
        {
            List<BlogPostModel> blogPosts = _blogPosts.FindAll(blogPost => blogPost.Label == tag);
            
            return blogPosts;
        } else {
            return _blogPosts;
        }
        }

        public static bool AddBlogPost(BlogPostModel blogPost)
        {
            blogPost.Id = ++_id;
            _blogPosts.Add(blogPost);
            return true;
        }

        public static bool DeleteBlogPost(int id)
        { 
           BlogPostModel blogPostModel =_blogPosts.FirstOrDefault(blogPost => blogPost.Id == id);
           if(blogPostModel != null) 
           {
           _blogPosts.Remove(blogPostModel);
            return true; 
           } else 
           {
            return false;
           }
        }

        public static bool ValidateBlogPostModel(BlogPostModel blogPost)
        {
            bool isValid = true;
            if (string.IsNullOrWhiteSpace(blogPost.Title))
            {
                isValid=false;
            }
            if (string.IsNullOrWhiteSpace(blogPost.Content))
            {
                isValid=false;
            }
            if (string.IsNullOrWhiteSpace(blogPost.Label))
            {
                isValid=false;
            }
            return isValid;
        }

        public static bool UpdateBlogPostById(BlogPostModel old_post, BlogPostModel new_post)
        {
            if(ValidateId(new_post.Id) && old_post.Id == new_post.Id) 
            { 
            old_post.Label = new_post.Label;
            old_post.Title = new_post.Title;
            old_post.Content = new_post.Content;
            return true;
            }
            return false;
        }

         public static bool ValidateId(int id)
        {
            if(id <= 0 || id > _id) 
            {
                return false;
            }
            return true;
        }


    }
}