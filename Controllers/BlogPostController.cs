using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using hello_blog_api.TestData;
using System.Net;
using System.Collections.Generic;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostController: Controller
    {
        ///<summary>
        ///Creates a blog using the payload information.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult CreateBlogPost([FromBody] BlogPostModel blogPost)
        {
            try
            {
                if (!BlogPostTestData.ValidateBlogPostModel(blogPost))
                {
                    return BadRequest("Model is not valid!");
                }
               
                if(!BlogPostTestData.AddBlogPost(blogPost))
                {   
                     return StatusCode((int)HttpStatusCode.InternalServerError);
                }
                
                return CreatedAtAction(nameof(CreateBlogPost), blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of all blogs.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public IActionResult GetAllBlogPosts()
        {
            try
            {
                List<BlogPostModel> blogPosts = BlogPostTestData.GetAllBlogPosts();
                return Ok(blogPosts);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public IActionResult GetBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog or a list of blogs based on the tag.
        ///Endpoint url: api/v1/BlogPost/tag/{blogTag}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("tag/{blogTag}")]
        public IActionResult GetBlogPostsByTag([FromRoute] string blogTag)
        {
            try
            {
                List<BlogPostModel> blogPosts = BlogPostTestData.GetBlogPostsByTag(blogTag);
                if (blogPosts.Count == 0)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPosts);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Deletes a blog post from the post list.
        ///Endpoint url: api/v1/BlogPost/del/{blogPostId}
        ///</summary>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("del/{blogPostId}")]
        public IActionResult DeleteBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                bool succes = BlogPostTestData.DeleteBlogPost(blogPostId);
                if (succes == false)
                {
                    return StatusCode((int)HttpStatusCode.NotFound, "The blog post doesn't exist!");
                }
                return Ok("The blog post has been deleted!");
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Deletes a blog post from the post list.
        ///Endpoint url: api/v1/BlogPost/del/{blogPostId}
        ///</summary>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("update/{blogPostId}")]
        public IActionResult UpdateBlogPostById([FromRoute] int blogPostId, [FromBody] BlogPostModel updatedBlogPost)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound, "The blog post with the specified id doesn't exist!");
                }
                if(!BlogPostTestData.ValidateBlogPostModel(updatedBlogPost))
                {
                    return BadRequest("The blog post is not valid!");
                }
                if(BlogPostTestData.UpdateBlogPostById(blogPost, updatedBlogPost)) 
                {
                    return Ok("The post has been updated!");
                }
                return BadRequest("Something bad happened! I guess you tried to change the id!");
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

     }
}